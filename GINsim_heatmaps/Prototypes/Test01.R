

#### Recupere les infos du modèle dans un dico... #######################

commando <- "java -jar"
modele <- 'Model_V2.zginml'

### D'abord la version de GINsim #################
ginsimVer <- function(){
  files <- list.files(".")
  versions <- grep(pattern = "GINsim", x = files, value = T)
  return(versions)
}


GINsim <- ginsimVer()

com <- paste(commando, ginsimVer(), "-s Test.py", modele, sep = "")


try(
  system(command = paste(commando, ginsimVer(), "-s Test.py", modele) ))
  
nodes <- read.table("Model_nodes.csv",sep = ",", stringsAsFactors = F)
types.nodes.maxi <- c("Type","Node_name","maxi")
colnames(nodes) <- types.nodes.maxi
nodes$Node_name <- gsub('\\s+', '', nodes$Node_name)



subsett <- subset(nodes, subset = nodes$Type == "Multivalued")

mylist <- split(nodes, nodes$Type)




identifier <- function(df){
  return(list(df[,2],df[,3]))
}

mega_identifier <- function(x){return(by(x, 1:nrow(x), identifier))}
mylist[[1]]

model_compendium <- lapply(mylist, FUN = mega_identifier)
Booleens <- lapply(model_compendium[['Boolean']], FUN = function(x){
  node <- x[[1]][1]
  return(node)
  }
)

getLabels <- function(part_compendium){
  labels <- lapply(part_compendium, FUN = function(x){
    return(x[[1]][1])
  })
  return(labels)
}

getValues <- function(part_compendium){
  values <- lapply(part_compendium, FUN = function(x){
    return(x[[2]][1])
  })
  return(values)
}


values <- getValues(model_compendium[['Boolean']])
labels <- getLabels(model_compendium[['Boolean']])

list.dirs(".")
any(grepl("attractors", list.dirs()))
grep("attractors", list.dirs())
grep("pp", list.dirs())
