#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import os
import sys
import itertools
from itertools import combinations
import re
import argparse
import pandas as pd
print(sys.version)
#Put the boolean nodes and multivalued nodes here


#Function generating files 1 par 1, containing the powerset of n nodes.
def combinator(compendiumfile,perturbfile,naturemut):
    print("Getting compendium...")
    compendium = getDict(compendiumfile)
    print(str(compendium))
    print("Getting perturbations list...")
    l_raw = list2mut(perturbfile)
    print("Adding tags to perturbations...")
    l_refined = lister(l_raw, compendium, naturemut)
    print("Building perturbations files...")
    combmaster(l_refined)
    return("done ^^")


### Construct dictionnary containing NatureofNode,NodesofthisNature, nature = Multivalued, boolean

def getDict(file):
    dict = {"Multivalued" : [], "Boolean" : []}

    df = pd.read_csv(file, header = None)
    df.columns =["Type","Node","Max_value"]
    print(df)

    nodes = df["Node"].str.strip()
    values = df["Max_value"]

    couples_node_value = list(zip(nodes,values))


    for e in couples_node_value:
        if e[1] > 1:
            dict["Multivalued"] += [e]
        else:
            dict["Boolean"] += [e]
    return(dict)


def combmaster(l):
    i = 1
    print(l)
    while i < len(l) + 1: #TO define manually, 5 is best for getting all the triple/quadruple mutants
        c = []
        c.extend(combinations(l,i))
        #print(len(c))
        if len(c) != 0:
            print(c)
            filename = "Mutants_list_%i.txt" % i
            comb2 = [list(row) for row in c]
            comb3 = nest2notnest(comb2)
            #print(comb3)
            print("Printing the file: " + filename)
            enumerer(filename,comb3)
        i += 1
    return("Done")


def listv2_simple(listmutant): #Takes a list of mutants, and places the KO label (Knock-Out)
    res = []
    for e in listmutant:
        res = res + [e + " KO"]
    return res



#Takes into account the KO or E1 nature
def lister(l,d, mutantNature):

    if mutantNature == "KO":
        return(listv2_simple(l))
    else:
        return(greffer2(l,d))



#If ectopic, we have to take into account the max value (considering only the max value atm)
def greffer2(l,d): #l = list of perturbations, d = Model compendium
    greffed = []
    print(d["Boolean"][0])
    for e in l:
        info = getNodeInfoInCompendium(e,d)
        print(info)
        nature = "E%i" % (info[1])
        greffed += [e + " " + nature]
    return(greffed)

def getNodeInfoInCompendium(str,d):
    for key in d:
        for t in d[key]:
            if str in t:
                return(t)



#Get List of mutants from Perturbations file
def list2mut(file):
    a = open(file,"r")
    lines = a.readlines()
    res = [item.strip() for item in lines]
    a.close()
    return res

def enumerer(file, l):
    a = open(file,"w")
    for e in l:
        a.write(e + "\n")
    a.close()
    return("Done")

# Core = Build a powerset
def powerset(iterable):
    "powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return itertools.chain.from_iterable(itertools.combinations(s, r) for r in range(len(s)+1))

# Transform iterable to list
def it2list(iterable):
    return(list(iterable))

def nest2notnest(nes): #Transforms a nested list to a simple list.
    res = []
    for e in nes:
        if len(e) == 1:
            res = res + e
        elif len(e) > 1:
            res = res + [','.join(e)] #concatenates the list as a string where the elements are separated by a comma.
        else:
            return(False)
    return res




######### MAIN ##########





compFile = sys.argv[1]
mutlistFile = sys.argv[2]
natureMut = sys.argv[3]

print(os.getcwd())
#Takes compendium, perturbation list and nature of mutation
combinator(compFile,mutlistFile,natureMut)
