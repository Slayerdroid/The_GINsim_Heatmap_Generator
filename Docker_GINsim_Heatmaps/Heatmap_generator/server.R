#
# This is the server logic of a Shiny web application. You can run the 
# application by clicking 'Run App' above.
#
# Find out more about building applications with Shiny here:
# 
#    http://shiny.rstudio.com/
#
# Libraries ------
#' @import data.table
#' @import ggplot2
#' @import tidyr
#' @import parallel
#' @import Matrix
#' @import gridExtra
#' @import reshape2
#' @import RColorBrewer
#' @import ggpubr
#' @import grid
#' @import shiny
#' @import shinyWidgets
#' @import shinydashboard
#' @import reticulate
#' @import here


### GLOBAL variables ----
COMMAND <- "java -jar"
pathbase <- getwd()
# For Firas' file demonstration:
HAMMAMI_2021 <- c("Fe_free","Fur","RyhB","H2O2","OxyR","Hpx","Suf","IscR_A","IscR_H","Isc","ErpA","NfuA")
Perturbz <- c("Fur","RyhB","OxyR","Hpx","Suf","IscR_A","IscR_H","Isc")
Pattern2match <- paste(HAMMAMI_2021, collapse = "|")
ReadoutsHammami <- c("Fe_free","H2O2","Isc","Suf","ErpA")
IS_Readouts <- c("Isc","Suf")
### Functions (Helper)  ###################################


#

#' Check that a version of GINsim is present.
#'
#' @param failed the verification
#' Displays a window if there is more than 1 version.
verify_selection <- function(failed = FALSE) {
  ns <- session$ns
  showModal(
    if (failed == F) #If there is more than one GINsim version, choose the version.
      modalDialog(title = "GINsim version",
                  selectInput(inputId = ns("test"), 
                              label = "Please choose your GINsim vers",
                              choices = NULL),
                  "Please choose your GINsim version",
                  footer = tagList(
                    actionButton(ns("ok"), "OK"),
                    modalButton("Cancel")
                    
                  )
      )
  )
}


#' Search for GINsim versions
#' @return  the GINsim versions as a vector
ginsimVer <- function(){
  if (grepl("Engine",getwd())){
    versions <- grep(pattern = "GINsim", x = list.files(), value = T)
    return(versions)
  } else {
    files <- list.files("./Engine")
    versions <- grep(pattern = "GINsim", x = files, value = T)
  }
  return(versions)
}


#' Get the node names of the selected model.
#' @param part_compendium a part of the model compendium (booleens, multivalues or inputs)
#' @return  the node labels (names if you prefer)
getLabels <- function(part_compendium){
  labels <- lapply(part_compendium, FUN = function(x){
    return(x[[1]][1]) 
  })
  return(labels)
}


#' Get the max value associated to the nodes variables
#' @param part_compendium a part of the model compendium (booleens, multivalues or inputs)
#' @return  the Max_i (maximum value for the nodes belonging to a category)
getValues <- function(part_compendium){
  values <- lapply(part_compendium, FUN = function(x){
    return(x[[2]][1])
  })
  return(values)
}


### Function to check if GINsim has been launched...
#' Checks whether GINsim has been launched before. GINsim generates 3 files when launched:
#' "info.txt","error.txt", "trace.txt"
#' @return  a vector with the 3 files if present, an empty vector otherwise.
GINsim_generated_files <- function(){
  ginsim_special_files <- c("info.txt", "error.txt","trace.txt")
  if (any(grepl(ginsim_special_files[1], list.files()))) {
    return(ginsim_special_files)
  } else {
    return(c())
  }
}


# Define server logic ----
shinyServer(function(input, output, session) {
  
  # # ##### Python part
  # virtualenv_dir = Sys.getenv('VIRTUALENV_NAME')
  # python_path = Sys.getenv('PYTHON_PATH')
  # 
  # # Create virtual env and install dependencies
  # reticulate::virtualenv_create(envname = virtualenv_dir, python = python_path)
  # reticulate::virtualenv_install(virtualenv_dir, packages = PYTHON_DEPENDENCIES)
  # reticulate::use_virtualenv(virtualenv_dir, required = T)
  
  
  # Functions and variable to help to build the model compendium ----
  
  #
  #' Ensure that a GINsim version is present in the Engine folder
  #' 
  #' @return  the GINsim version if there is only one of it.
  checkGINsimVer <- function()
  {
    version <- ginsimVer()
    if(length(version) == 1) # If there's only one version, choose that one
    {
      return(version)
    } 
    else if(length(version) > 1) #If there's more, choose between them
    {
      verify_selection()
    }
    else # No version ? No simulation :)
    {
      message("A GINsim jar file is required. Please put the jar file in the folder.")
      return(NULL)
    }
  }
  
  #GINsim version (character)
  ginsim_version <- checkGINsimVer()
  
  # Input file, which serves to build the model compendium
  inFile <- reactive({
    input$file1
  })
  
  # GINsim results, after perturbations. They are in "XXX_attractors" folders
  sim_results <- reactive({
    any(grepl("attractors",list.dirs()))
  })
  
  
  reactive({
    if (is.null(inFile()))
      return(NULL)
  })


  
  #Command to get the compendium.
  command <- reactive({paste(COMMAND,
                             checkGINsimVer(),
                             "-s",
                             "Test.py",
                             inFile()$datapath)})
  

  # Cheatcode for getting the files into the Engine folder (containing the core scripts for the analysis) )
  path2 <- paste0(pathbase,"/Engine")
  
  #

  #' Generates the model compendium. The compendium is a dictionnary (k,v), where: 
  #' k = node type (inputs, internal boolean, internal multivalued )
  #' v = [Node name, Max_i] (max_i = maximal value for the variable i)
  #' @return  the model compendium.
  compendium <- function()
  {
    validate(
      need(!is.null(inFile()),"Please load the model file...") 
    )
    
    print(getwd())
    print(command())
    
    setwd(path2)
    # Read the model informations
    system(command(), intern = T)
    
    
    #Read the model nodes file generated by the command
    nodes <- read.table("Model_nodes.csv",sep = ",", stringsAsFactors = F)
    types.nodes.maxi <- c("Type","Node_name","maxi") #Labels
    colnames(nodes) <- types.nodes.maxi
    nodes$Node_name <- gsub('\\s+', '', nodes$Node_name) #remove spaces
    nodes_by_type <- split(nodes, nodes$Type) # Separate the nodes by type
    identifier <- function(df){
      return(list(df[,2],df[,3]))
    }
    
    
    mega_identifier <- function(x){
      return(by(x, 1:nrow(x), identifier))
      }
    
    model_compendium <- lapply(nodes_by_type, FUN = mega_identifier)
    return(model_compendium)
  }

#'
#'
  
  
  # Test d'alignement des boutons / For future usage
  # tweaks <- list(tags$head(tags$style(HTML("
  #                                .multicol { 
  #                                  height: 150px;
  #                                  -webkit-column-count: 5; /* Chrome, Safari, Opera */ 
  #                                  -moz-column-count: 5;    /* Firefox */ 
  #                                  column-count: 5; 
  #                                  -moz-column-fill: auto;
  #                                  -column-fill: auto;
  #                                } 
  #                                ")) 
  #   ))
  
  
  
  
  # Allows to check the input file path (Diagnosis)
  # output$file <- renderTable({inFile()})
  
  
  #output$file_loc <- renderText({ inFile})
  
  
  #### Useful variables which will be used in the UI #####################
  
  
  # command allowing the GINsim perturbations simulations.
  command_Masterscript_analysis <- reactive({c( 
                                                   "MasterScript.py", 
                                                   ginsim_version, 
                                                   inFile()$datapath,
                                                   "Model_nodes.csv",
                                                   "Perturbations",
                                                   input$mutation)
  })
  
  # Reactive associated to the building of the model compendium (changes if the input file changes)
  
  model_compendium <- reactive({compendium()}) #Model compendium (dictionnary with keys = node type, values = nodeinfo (list containing node name + max i))
  
  # List of inputs (k,v)
  #Inputs <- reactive({getLabels(model_compendium()$Inputs)})
  Inputs <- reactive({unlist(getLabels(model_compendium()$Inputs), use.names = F)})
  ## Booleens & Multivalues = internal nodes & outputs.
  Booleens <- reactive({unlist(getLabels(model_compendium()$Boolean), use.names = F)})
  Multivalues <- reactive({unlist(getLabels(model_compendium()$Multivalued), use.names = F)})
  ## get all internal nodes & outputs.
  internal_nodes <- reactive({
    boolKV <- as.list(setNames(Booleens(),Booleens()))
    multKV <- as.list(setNames(Multivalues(),Multivalues()))
    c(boolKV,multKV)
  })
  
  #### Outputs ##########
  
  # For diagnosis
  #output$command <- renderText({command() })
  
  
  ### Renders the nodes informations with 3 categories: Inputs, Internal boolean, Internal multivalued
  output$ibox <- renderUI({

    column(6,
           box(
             title = "Model Inputs",
             HTML(paste(Inputs(), sep = "", collapse = '<br/>')),
             width = NULL
             #icon = icon("credit-card"),
           ),
           if (length(Booleens()) > 0)
           {
             box(
               title = "Regulated Boolean Nodes",
               HTML(paste(Booleens(), sep = "", collapse = '<br/>')),
               width = NULL
             )
           },
           if (length(Multivalues()) > 0) 
           {
             box(
               title = "Regulated Multivalued Nodes",
               HTML(paste(Multivalues(), sep = "", collapse = '<br/>')),
               width = NULL
             )
           }
    )
  })
  
  #### Perturbations parameters #########
  
  # Displays the perturbations parameters (KO or ectopic, and if the user wants to mutate all internal nodes or not).
  output$sim_params <- renderUI({
    validate(
      need(!is.null(inFile()),"...") 
    )
    
    tagList(
    box(title = "Perturbation type",
      radioButtons("mutation", 
                   "Type of mutation:", 
                   choiceNames = list("KO","Ectopic (max value)"),
                   choiceValues = list("KO", "Em")
      )
      ),
    
    box(title = "Nodes to perturb",
      radioButtons("AllMut", 
                   "Mutate all internal nodes ?", 
                   choiceNames = list("Yes","No"), 
                   choiceValues = list(TRUE,FALSE),
                   selected = FALSE),
      
      conditionalPanel("input.AllMut == 'FALSE'",
                       if (all(grepl(Pattern2match,internal_nodes()))) #Firas' model:
                       {
                         checkboxGroupInput(
                           "perturbations", "Choose your perturbations",
                           choices = internal_nodes(),
                           selected = Perturbz,
                           inline = F
                         )
                       } else { #Not Firas' Model:
                         checkboxGroupInput(
                           "perturbations", "Choose your perturbations",
                           choices = internal_nodes(),
                           inline = F
                         )  
                       }
                       
      )
      
    )
    )
  })
  

  # Variable for the selected perturbations
  perturb <- reactive({
    if(input$AllMut){
      perturb <- internal_nodes()
    } else {
      perturb <- input$perturbations
    }
  })

  
  # Button for launching the analysis.
  output$launcher <- renderUI({
    
    validate(
      need(!is.null(inFile()),"...")
    )
    box(title = "Launch the analysis",
        actionButton("analysis", "Let's use GINsim!")
    )
  })
  
  
  
  
  ##### GINsim Perturbations and simulations. ####
  observeEvent(input$analysis, {
    
    #Progress bar ...
    req(input$analysis)
    progress <- Progress$new(max = 5)
    on.exit(progress$close())
    progress$set(message = "Purging the cache...")
    
    print("PUUUUUUUURGE")
    ## Purge all the useless directories
    generated_folders <- tail(list.dirs(), length(list.dirs()) -1)
    if (length(generated_folders) > 0)
    {
      sapply(generated_folders, unlink, recursive = T)
    }
    ## Purge all the useless files
    generated_files <- grep("Mutant_",list.files(), value = T)
    to_delete <- c(generated_files, GINsim_generated_files())
    print(to_delete)
    if(length(to_delete)>0)
    {
      print(to_delete)
      sapply(to_delete, unlink, recursive = T)
    }
    
    progress$inc(1)
    
    progress$set(message = "Preparing the simulations")
    #Get all nodes to perturb
    nodes4perturb <- unlist(unname(perturb()))
 
    print(getwd())
    print(command_Masterscript_analysis())
    
    progress$inc(1)
    # Write in a file the selected perturbations
    writerEngine(nodes4perturb, "Perturbations")
   
    progress$inc(1)
    progress$set(message = "Performing all simulations & perturbations on GINsim...")
    #Generate all the perturbations combinations + GINsim simulations.
    system2("python3", command_Masterscript_analysis())
    progress$inc(1)
  })
  
  
  
  perturbations_file <- "./Engine/Perturbations"
  
  # Function for writing the perturbations!
  writerEngine <- function(l,filename)
  {

    write(l,filename)
  }
  

               
  ########### Heatmap Generation section ###################
  
  model_outputs <- reactive({input$readouts})
  is_outputs <- reactive({input$IS_outputs})
  multi_inputs <- reactive({input$model_inputs}) # Only if more than 2 inputs in the compendium
  
  zip_file <- reactiveVal()
  is_zip_file_present <- reactiveVal(FALSE)
  pdf_files <- reactiveVal()
  
  get_zip_files <- function(){ 
    if (grepl("Engine",getwd())){
      return(grep("*.zip", list.files("."), value = T))
    } else {
      return(grep("*.zip", list.files("./Engine"), value = T))
    }
    
    }
  
  presence_zip_files <- function(){
    if (grepl("Engine",getwd())){
      return(any(grepl(".zip", list.files(getwd()))))
    } else {

      return(any(grepl(".zip", list.files("./Engine"))))
    }
  
  }
  
  get_pdf_files <- function(){ 
    if (grepl("Engine",getwd())){
      return(grep("*.pdf", list.files("."), value = T))
    } else {
      return(grep("*.pdf", list.files("./Engine"), value = T))
    }
  
  }
  

  # UI displaying the Heatmap generation parameters (readouts and mutants of interest, called IS)
  output$par_analysis <- renderUI({
    validate(
      need(sim_results(),"Please perform the GINsim perturbations first")
    )
    
    column(12,
           if (length(Inputs()) > 2)
           {
             box(
               checkboxGroupInput(
                 "model_inputs", "Choose 2 inputs amongst them",
                 choices = Inputs()
                 
               )
             )
            },
           
           if (all(grepl(Pattern2match,internal_nodes()))) #Only For Firas' Model:
           {
             box(
               checkboxGroupInput(
                 "readouts", "Choose your readouts",
                 choices = internal_nodes(),
                 selected = ReadoutsHammami,
               )
             )
             
           } else { #If not Firas' Model:
             box(
               checkboxGroupInput(
                 "readouts", "Choose your readouts",
                 choices = internal_nodes(),
                 selected = internal_nodes()
               )
             )
           },
           
           box(
             radioButtons("IS", 
                          "Do you want to check for mutants satisfying your chosen phenotypes ?", 
                          choiceNames = list("Yes","No"), 
                          choiceValues = list(TRUE,FALSE)),
             conditionalPanel("input.IS == 'TRUE'",
                              if (all(grepl(Pattern2match,internal_nodes()))) #Only For Firas' Model:
                              {
                                checkboxGroupInput(
                                    "IS_outputs", "Choose your phenotypes (value at 0 for this version)",
                                    choices = internal_nodes(),
                                    selected = IS_Readouts,
                                    inline = F)  
                              } else {  #If not Firas' Model:
                                checkboxGroupInput(
                                  "IS_outputs", "Choose your phenotypes (value at 0 for this version)",
                                  choices = internal_nodes(),
                                  inline = F)
                              }
             )
           )
           
    )

    
    
  })
  
  
  
  check_inputs <- reactive({
    return((length(Inputs()) == 2) || ((length(Inputs()) > 2) && (length(multi_inputs()) == 2))
  ) })
  

  # Button to generate the heatmaps
  output$pdf_generator <- renderUI({
    
    validate(
      need(sim_results(),"Please perform the GINsim analysis first"),
      need(length(Inputs()) > 1, "Sorry, the app does not handle only 1 Input..."),
      need(check_inputs(), "Please select 2 inputs amongst the list!"),
      need(length(model_outputs())> 0, "Please select at least one readout")
      
    )
    box(title = "Generate the heatmaps",
        actionButton("Heatmap", "Generate heatmaps!")
    )
  })
  
  # Button for downloads
  

  output$download <- renderUI({
    
    
    validate(
      need(is_zip_file_present(),"Please generate the heatmaps first!!")
    )
    box(title = "Download the heatmaps!",
        downloadButton("download_pdf", "Download the results (.zip)")
    )
    
  })

  # Download the pdf files generated as a zip format.
  output$download_pdf <- downloadHandler(
    filename = function() {
      grep(".zip", list.files(), value = T)
      },
    content = function(file) {
      
      file.copy(grep(".zip", list.files(), value = T), file)
      
 #     unlink(grep(strsplit((inFile()[[1]][[1]]), "\\.")[[1]][[1]], list.dirs()), recursive = TRUE)

 
    }
  )
# Launching the IsolatorMapper & Helper -----------------------------------

  # Generate the heatmaps!!
  observeEvent(input$Heatmap, {
    
    # Progress bar
    req(input$Heatmap)
    progress <- Progress$new(max = 20)
    on.exit(progress$close())
    progress$set(message = "Heatmap generation from GINsim results in progress...")
    
    #First, we get all the folders with the GINsim results!
    Folderz <- list.dirs("./")
    Folders <- grep(pattern = "Mutants_list",x = Folderz, value = T)            

    # Get all the prerequisite for the IsolatorMapper.R script.
    NAMES <- readLines("Model_labels.txt")
    if (length(Inputs()) == 2) {
      INPUTS <- Inputs()
    } else {
      INPUTS <- multi_inputs() 
    }
    BOOLEAN <- Booleens()
    MULTIVALUED <- Multivalues()
    THEOUTPUTS <- model_outputs()
    IS.OUTPUTS <- is_outputs()
    ### Do not change yet... For the moment it's suited for multivalued nodes where Max_i = 2
    VALUESFUR <- c(1,2)
    VALUESMULTI <- c(0,1,2)
    VALUESBOOL <- c(0,1)
    ## Colorscales
    COLORSCALE_MULTI <- c("#ffffff","#c8afff", "#cc00ff")
    COLORSCALE_BOOL <- c("#ffffff","#cc00ff")
    DARK_COLORSCALES <- list(COLORSCALE_BOOL,COLORSCALE_MULTI)
    progress$inc(2)
    
    
    # Beginning of the analysis + heatmap generation. Loading first the source files
    source("IsolatorHelper.R", local = T)
    source("IsolatorMapper.R", local = T)
    progress$inc(2)

    allmutNames <- getAllMutants(Folders)
    progress$inc(2)
    
    ALLMUT <- lapply(Folders, reader) #Get all csv files from folders
    #print(ALLMUT[[1]][[1]])
    ALLMUT <- G_Formatter(ALLMUT,NAMES) #Formatting all the tables.
    #WT <- ALLMUT[[1]][[length(ALLMUT[[1]])]]

    progress$inc(2)
    
    
    # Plot a sample to see an example (Displayed at the end of the analysis)
    #WT_sub <- DarkIsolator(WT,INPUTS,THEOUTPUTS[1])
    #p <- Darkplotter(WT_sub,INPUTS)
    # output$plot <- renderPlot({
    #   p
    # })
    progress$inc(2) #### 5
    # Build all the plots for the selected outputs 
    AllPlots <- DarkMegaIsolatorPlotter(ALLMUT,allmutNames, INPUTS,  THEOUTPUTS)
    progress$inc(4) ####
    
    # Print all the plots for each mutant in mutant list
    if (length(ALLMUT) == 1 && length(ALLMUT[[1]]) == 2)
    {
      named_mutant <- sub(" ", "_", allmutNames[[1]][[1]])
      filename <- paste0(named_mutant,".pdf")
      ThePlotPrinter(AllPlots,allmutNames,filename)
    } else {
      ULTIMATE_PRINTER(AllPlots,allmutNames,Folders)
    }
    
    progress$inc(4)
    
    # If you specified that you want conditions where one or several nodes are off in the attractor:
    if (length(IS.OUTPUTS)>0)
      {
      
      # Searching for these conditions
      conditions_detected <- Search4Mutants(allmutNames,ALLMUT,IS.OUTPUTS, NAMES, Folders)
      print("Deleting the huge structure first...")
      print(object.size(ALLMUT))
      rm(ALLMUT) # Memory trick to avoid RAM excess
      # Remove the big structure for the first plots
      rm(AllPlots)
      print(object.size(conditions_detected))
      print("Removal done")
      
      # Generate all the plots for the found conditions.
      All_IS_Plots <- DarkMegaIsolatorPlotter_IS(conditions_detected[[1]],conditions_detected[[2]], INPUTS, IS.OUTPUTS)
      if (length(All_IS_Plots) == 0)
      {
        print("Nope !")
        progress$set(message = "No Mutant with your phenotype was found")
        progress$inc(1)
      } else {
        progress$set(message = "Mutants with your phenotype were found, proceeding to the heatmap generation for them...")
        # Print all the plots for each mutant found!
        ULTIMATE_PRINTER_IS(All_IS_Plots,conditions_detected)
        progress$inc(1)
      }
    }
    parameters_used <- c("Readouts", THEOUTPUTS , "Phenotypes looked for (value = 0)", IS.OUTPUTS)
    write(parameters_used, "Heatmaps_parameters.txt")
    print("D O N E")
    # print(pdf_files())
    
    
    #### Generate the zip file with all the results + folder clean
    
    to_delete <- c("error.txt","trace.txt","info.txt","Perturbations")
    unlink(to_delete)
    already_here <- grep(".zip", list.files(), value = T)
    unlink(already_here)
    if (length(grep(strsplit((inFile()[[1]][[1]]), "\\.")[[1]][[1]], list.dirs())) > 0) 
    {
      folders_to_remove <- grep(strsplit((inFile()[[1]][[1]]), "\\.")[[1]][[1]],list.dirs(), value = TRUE)
      print(folders_to_remove)
      unlink(folders_to_remove, recursive = TRUE, force = TRUE)
    }
    dirname <- paste0(strsplit((inFile()[[1]][[1]]), "\\.")[[1]][[1]],"_",format(Sys.time(), format="%Y-%m-%d-%H.%M.%S"))
    dir.create(file.path("./",dirname))
    dest <- paste0(dirname,"/", get_pdf_files())
    #Move all files to give to the user in the final folder
    file.rename(get_pdf_files(),dest)
    file.rename("Heatmaps_parameters.txt",paste0(dirname,"/","Heatmaps_parameters.txt"))
    # choose the list_mutants text files
    not_chosen <- sub("\\./", "", list.dirs())[2:length(list.dirs())]
    chosen <- grep("Mutants_list", list.files()[which(!list.files() %in% not_chosen)], value = T)
    dest2 <- paste0(dirname,"/",chosen)
    file.copy(chosen, dest2)
    folder_to_zip <- grep(dirname, list.dirs(), value = T)
    zip(paste0(dirname,".zip"), folder_to_zip)
    zip_file(get_zip_files())
    is_zip_file_present(presence_zip_files)
  }
               )
  

  
  
  #### Node info display ----
  
  output$Boolean <- renderUI({
    Booleens <- unlist(getLabels(model_compendium()$Boolean))
    #print(model_compendium())
    HTML(paste(Booleens, sep = "", collapse = '<br/>'))
    
  })
  
  output$Multivalued <- renderUI({
    Multivalues <- unlist(getLabels(model_compendium()$Multivalued))
    HTML(paste(Multivalues, sep = "", collapse = '<br/>'))
  })
  
  output$Model_Inputs <- renderUI({
    Inputs <- unlist(getLabels(model_compendium()$Inputs))
    HTML(paste(Inputs, sep = "", collapse = '<br/>'))
  })
  
})

