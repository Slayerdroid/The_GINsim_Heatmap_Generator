# GINsim_heatmap scripts

## First and foremost, huge update!

I've built an app which is able to perform the things done with my scripts.

Please follow the TOC for the requisites & tutorials.

[[_TOC_]]

## Pre-requisites

### Hardware requirements:

* 16 GB RAM 
* 4 cores CPU (I could launch my analysis on my older laptop, which has the "Intel® Core™ i5-4200H CPU @ 2.80GHz × 4" )

### For Linux users (Ubuntu 18.04 +)


### Software requirements:

This folder contians all the scripts used for analyzing a model with two inputs, with my GINsim model as an example (Examples/Hammami2020.zginml).
If you use another GINsim example, ***please check that the single KO and E1 mutants are already configured beforehand!!***

#### For All users, the easy way (With Docker)

* Docker (https://docs.docker.com/get-started/)

When you have installed Docker, you just have to follow the "How to launch the Docker version" section.

#### For Linux Users (Ubuntu 18.04 and 20.04, without Docker)

* conda 4.7.11 (did not test for earlier releases, but you should be able to use miniconda though)
* If you do not have conda (or miniconda) on your computer, please refer to the official website.
* Works well with Ubuntu 20.04 (the conda environment worked well with that version)

##### Packages required (if you don't want to use conda)

* python3
* R https://cran.r-project.org/
* openjdk-11
* ghostscript
* zip

If you do not have zip, openjdk & ghostscript, please install them:

```
sudo apt install zip openjdk-11-jre-headless ghostscript
```

For the R packages required, they are the following:

* data.table
* ggplot2
* tidyr
* parallel
* Matrix
* gridExtra
* reshape2
* RColorBrewer
* ggpubr
* grid
* shiny
* shinyWidgets
* shinydashboard
* reticulate
* here

## How to launch the app (Non-Docker version, with the conda environment):

First, clone the git:

```
git clone https://gitlab.com/Slayerdroid/Scripts.git
cd The_GINsim_Heatmap_Generator
```

Second, prepare the conda environment:

```
cd GINsim_heatmaps/

conda env create -f TestGINsim.yml

source activate TestGINsim

```

Finally, launch the app!

```
Rscript Launcher.R
```
You will be able to access a web page from the address displayed in the terminal, which correspond to the app UI.




***NB: if you have only 8 Gb RAM, please close most of the apps before launching the analysis!!***

## How to launch the Docker version

1. From the root of the repo, go to the docker folder:

```
cd Docker_GINsim_Heatmaps
```
2. Build the docker image through the dockerfile in the folder, before running the container:
```
docker build --tag testginsim .
docker run -d -p 3838:3838 --name heatmapgenerator testginsim
```

3. Open a webpage and go to localhost:3838


# How to use the app

In the app, the protocol is the following:

1. Load the model (with the example found in The_GINsim_Heatmap_Generator/Examples/Hammami_2020.zginml)
2. Choose your mutation type (it only does all KO mutants or all ectopic mutants, no mix atm), and whether you want to perform the perturbations on all internal nodes or not.
3. Click on the bottom right button to launch the analysis (on the model delivered with the app, you have to wait 5-15 minutes)
4. Click on the "Simulation analysis" tab on the left, and choose your readouts. You can also select if you want the app to find mutants where the values of one or several model nodes are at 0 to isolate them.
5. Click on the button "Launch Analysis". The process may take 15-30 minutes depending of your RAM and processor. 


If you want to reproduce my results, then:

1. (Same as the first step described previously)
2. Choose all KO mutants
3. (same as step 3 previously)
4. Choose Fe_free, H2O2, Isc, Suf, ErpA as readouts. Select Isc and Suf as phenotypes with mutants to search for.
5. (same as step 5 previously)

## Troubleshootings (and TO DO LIST)

> Oh noes the R script is too slow how do i make it faster

It is possible to launch it on a cluster, however the server stuff is not implemented yet. Moreover, you need to be careful: use more processors if you have enough RAM to do it!

In the meantime, it is possible to edit the IsolatorHelper.R script at the mc.cores line, and choose the number of cores used. (I should make an option for this...)

> It works only with models containing 2 inputs... What to do if we have more ?

I still need to implement the input choice (for models with 3+ inputs) (is extensible to more couples, but that extension is mostly RAM-dependent)

TO DO:

* Clean the generated files on close (and on startup)
